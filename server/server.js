Cloudinary.config({
    cloud_name: 'dowcdhkuo'
    ,api_key: '629881757164787'
    ,api_secret: '8xN_cDN-LMeb9l0vj1CBT7y6fmo'
});

Meteor.methods({
   fetchFromOSM: function(address) {
        address = encodeURI(address.split(' ').join('+'));
        var result = undefined;

        //http://wiki.openstreetmap.org/wiki/Nominatim
        //http://wiki.openstreetmap.org/wiki/Nominatim_usage_policy

        // mapquest.com
        result = Meteor.http.get("http://open.mapquestapi.com/nominatim/v1/search.php",{
            headers: {
                "User-Agent": "Meteor/1.0"
            },
            params:{
                 key: "Fmjtd|luub2hu7ng,8g=o5-9uzgqu"
                ,q: address
                ,format: "json"
                ,polygon: 0
                ,addressdetails: 1
                ,viewbox: "-78.55,39.62,-75.16,38.03"
                ,bounded: 1
            }
        });

        // openstreetmap.org
        // result = Meteor.http.get("http://nominatim.openstreetmap.org/search",{
        //     headers: {
        //         "User-Agent": "Meteor/1.0"
        //     },
        //     params:{
        //         //key: "Fmjtd|luub2hu7ng,8g=o5-9uzgqu"
        //          q: address
        //         ,format: "json"
        //         ,polygon: 0
        //         ,addressdetails: 1
        //         ,viewbox: "-78.55,39.62,-75.16,38.03"
        //         ,bounded: 1
        //     }
        // }); 

        // opencagedata - this response is different
        // result = Meteor.http.get("http://api.opencagedata.com/geocode/v1/json",{
        //     headers: {
        //         "User-Agent": "Meteor/1.0"
        //     },
        //     params:{
        //         key: "61ebf9d264cbffca1c3892a1542da733"
        //         ,query: address
        //         ,pretty: 1
        //     }
        // }); 
    
        console.log(result);

        return result;
    },
    fetchFromOSMReverse: function(coordinates){
        var result = undefined;
       
        // mapquest.com  
        // result = Meteor.http.get("http://open.mapquestapi.com/nominatim/v1/reverse.php",{
        //     headers: {
        //         "User-Agent": "Meteor/1.0"
        //     },
        //     params:{
        //          key: "Fmjtd|luub2hu7ng,8g=o5-9uzgqu"
        //         ,lat: coordinates[1]
        //         ,lon: coordinates[0]
        //         ,format: "json"
        //         ,addressdetails: 1
        //         ,zoom: 18
        //     }
        // });

        // mapquest.com  
        result = Meteor.http.get("http://nominatim.openstreetmap.org/reverse",{
            headers: {
                "User-Agent": "Meteor/1.0"
            },
            params:{
                 lat: coordinates[1]
                ,lon: coordinates[0]
                ,format: "json"
                ,addressdetails: 1
                ,zoom: 18
            }
        });

        console.log(result);

        return result;
    },
    fetchFromCensus: function(address){
        // http://geocoding.geo.census.gov/geocoder/locations/onelineaddress?address=4600+Silver+Hill+Rd%2C+Suitland%2C+MD+20746&benchmark=9&format=json
        var url = "http://geocoding.geo.census.gov/geocoder/locations/onelineaddress?address=" + address + "&benchmark=9&format=json";
        var result = Meteor.http.get(url);
        //console.log(result);
        return result; 
    },
    fetchFromFCC: function(coordinates){
        var url = 'http://data.fcc.gov/api/block/find?format=json&showall=true&latitude=' + coordinates[1] + '&longitude=' + coordinates[0];
        var result = Meteor.http.get(url)  
        //console.log(result);
        return result;              
    },
    fuzzyAddress: function(address){
        var key = 'c55c611dcb45b258d3d5965344b59dc42398068';
        return key;
    }

});

