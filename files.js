FS.debug = true;

// function loadjscssfile(filename, filetype) {
//     if (filetype == "js") { //if filename is a external JavaScript file
//         var fileref = document.createElement('script')
//         fileref.setAttribute("type", "text/javascript")
//         fileref.setAttribute("src", filename)
//     } else if (filetype == "css") { //if filename is an external CSS file
//         var fileref = document.createElement("link")
//         fileref.setAttribute("rel", "stylesheet")
//         fileref.setAttribute("type", "text/css")
//         fileref.setAttribute("href", filename)
//     }
//     if (typeof fileref != "undefined")
//         document.getElementsByTagName("head")[0].appendChild(fileref)
// }

var baseUrl = "";

if (Meteor.isServer) {
    baseUrl = process.env.PWD;
    var ffmpeg = Meteor.npmRequire('fluent-ffmpeg');
}

var createThumb = function(fileObj, readStream, writeStream) {
    console.log('making thumbnail for ' + fileObj._id);
    if (fileObj.type().indexOf('image') > -1 ) {
        // Transform the image into a 100x100px thumbnail
        gm(readStream, fileObj.name())
          .size({bufferStream: true}, FS.Utility.safeCallback(function (err, size) { 
            if (err){
              console.log(err);
            }
          })).autoOrient().resize('100', '100').stream().pipe(writeStream);


    } else if (fileObj.type().indexOf('video/mp4') === 0 ){
      var file = writeStream.path.split('/').slice(-1)[0]
      file = file.substring(0,file.length-3) + 'mp4';
      ffmpeg( baseUrl + '/uploads/' +  file )
          .format('gif')
          .size('100x?')
          .duration('0:02')
          .writeToStream(writeStream, { end: true });
    } else {
      return false;
    }
};

var createMedium = function(fileObj, readStream, writeStream) {
    console.log('making medium image for ' + fileObj._id);
    if (fileObj.type().indexOf('image') > -1 ) {
        // Transform the image into a 100x100px thumbnail
        gm(readStream, fileObj.name())
          .size({bufferStream: true}, FS.Utility.safeCallback(function (err, size) { 
            if (err){
              console.log(err);
            }
          })).autoOrient().resize('500', '500').stream().pipe(writeStream);
    } else if (fileObj.type().indexOf('video/mp4') === 0 ){
      var file = writeStream.path.split('/').slice(-1)[0]
      file = file.substring(0,file.length-3) + 'mp4';
      ffmpeg( baseUrl + '/uploads/' +  file )
          .format('gif')
          .size('100x?')
          .duration('0:02')
          .writeToStream(writeStream, { end: true });
    } else {
      return false;
    }
};


var fixOrientation = function(fileObj, readStream, writeStream) {
    //console.log(fileObj.type());
    if (fileObj.type().indexOf('image') > -1) {
        gm(readStream, fileObj.name()).size({bufferStream: true}, FS.Utility.safeCallback(function (err, size) { 
            if (err){
              console.log(err);
            }

          })).autoOrient().stream().pipe(writeStream);
    } else{
        readStream.pipe(writeStream);        
    }
};

var beforeWrite = function (fileObj) {
    // var post = Posts.findOne(fileObj.postId);
    // if (post.type==='Dog') return {name:slugify(post.name)};
    // else return {name:slugify(post.title)};
    if (fileObj.type() == 'video/mp4'){
      fileObj.name(fileObj.name().substring(0, fileObj.name().length -3) + 'gif', {store: 'thumbs'});
      fileObj.extension('gif', {store: 'thumbs'});
      //fileObj.size(100, {store: 'thumbs'});
      fileObj.type('image/gif', {store: 'thumbs'});  
    }
};

var beforeWriteMedium = function (fileObj) {
    // var post = Posts.findOne(fileObj.postId);
    // if (post.type==='Dog') return {name:slugify(post.name)};
    // else return {name:slugify(post.title)};
    if (fileObj.type() == 'video/mp4'){
      fileObj.name(fileObj.name().substring(0, fileObj.name().length -3) + 'gif', {store: 'medium'});
      fileObj.extension('gif', {store: 'medium'});
      //fileObj.size(100, {store: 'thumbs'});
      fileObj.type('image/gif', {store: 'medium'});  
    }
};


var beforeWriteMain = function (fileObj) {
    // var post = Posts.findOne(fileObj.postId);
    // if (post.type==='Dog') return {name:slugify(post.name)};
    // else return {name:slugify(post.title)};
    if (fileObj.type() == 'video/quicktime'){
      fileObj.name(fileObj.name().substring(0, fileObj.name().length -3) + 'mp4', {store: 'eventPhotos'});
      fileObj.extension('mp4', {store: 'eventPhotos'});
      //fileObj.size(100, {store: 'thumbs'});
      fileObj.type('video/mp4', {store: 'eventPhotos'});  
    }
};

var eventPhotosStore = new FS.Store.FileSystem('eventPhotos', {
    path: baseUrl + '/uploads',
    transformWrite: fixOrientation,
    //beforeWrite: beforeWriteMain
});

var thumbsStore = new FS.Store.FileSystem('thumbs', {
    path: baseUrl + '/uploads/thumbs',
    transformWrite: createThumb,
    beforeWrite: beforeWrite

});

var mediumStore = new FS.Store.FileSystem('medium', {
    path: baseUrl + '/uploads/medium',
    transformWrite: createMedium,
    beforeWrite: beforeWriteMedium

});

eventPhotos = new FS.Collection('eventPhotos', {
    stores: [eventPhotosStore, thumbsStore, mediumStore],
      // filter: {
      //   maxSize: 31457280,
      //   allow: {
      //       //contentTypes: ['image/*', 'video/*', 'audio/*'],
      //       extensions: ['png', 'PNG', 'jpg', 'JPG', 'jpeg', 'JPEG','GIF', 'gif', 'MOV', 'mov', 'mp4', 'MP4', 'mp3', 'MP3', 'M4A', 'm4a', 'm4v', 'M4V']
      //   }
      // }
});

eventPhotos.on('stored', Meteor.bindEnvironment(function(fileObj, storeName) {
  if (fileObj.type() === 'video/mp4'){
    eventPhotos.update({_id: fileObj._id}, {$set: { ready: true}});
    events.update({"file._id": fileObj._id}, {$set: {file_name: fileObj.name(), video_ready: true}});

  }
  if (fileObj.type().indexOf('video') > -1 && fileObj.type().indexOf('video/mp4') == -1 ) {
    var Fiber = Npm.require('fibers');
    var fs = Npm.require('fs');

    var run_ffmpeg = function(callback){
        ffmpeg(fileObj.collection.primaryStore.path + '/eventPhotos-' + fileObj._id + '-' + fileObj.original.name)
          .audioCodec('aac')
          .format('mp4')
          .videoBitrate('512k')
          .videoCodec('libx264')
          .audioBitrate('64k')
          .audioChannels(1)
          .on('end', function(){console.log('done encoding');callback();})
          .on('error', function(err) {
            console.log('an error happened: ' + err.message);
          })
          // save to file
          .save(fileObj.collection.primaryStore.path + '/eventPhotos-' + fileObj._id + '.mp4' );
    };

    var syncfunction = Meteor.wrapAsync(run_ffmpeg)
    var crap = function(){
        try {
          var temp = new FS.File(fileObj.collection.primaryStore.path + '/eventPhotos-' + fileObj._id + '.mp4' )
          var temp1 = eventPhotos.insert(temp);
          fs.unlinkSync(fileObj.collection.primaryStore.path + '/eventPhotos-' + fileObj._id + '.mp4');
          eventPhotos.remove(fileObj._id);
          events.update({"file._id": fileObj._id}, {$set: {file_name: temp.name(), video_ready: true}});
          
        } catch (err) {
          console.log(err);
      }  
    };

    Fiber(function(){
      var result = syncfunction(crap);    
    }).run();

  } 

}, function() { console.log('Failed to bind environment'); }));


eventPhotos.allow({
    insert: function() {
        return true;
    },
    update: function() {
        return true;
    },
    remove: function() {
        return true;
    },
    download: function() {
        return true;
    }
});
