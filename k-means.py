from pymongo import MongoClient
import math
from collections import defaultdict
import json
from pprint import pprint
 
local = True

# Connect to mongo
try: 
	client = MongoClient('localhost', 27017)
	local = False
except:
	client = MongoClient('localhost', 3001)

if local:
	db = client.meteor
else:
	db = client.detour


collection = db.assets
clusters = db.clusters


def distance_on_unit_sphere(lat1, long1, lat2, long2):
 
    # Convert latitude and longitude to 
    # spherical coordinates in radians.
    degrees_to_radians = math.pi/180.0
         
    # phi = 90 - latitude
    phi1 = (90.0 - lat1)*degrees_to_radians
    phi2 = (90.0 - lat2)*degrees_to_radians
         
    # theta = longitude
    theta1 = long1*degrees_to_radians
    theta2 = long2*degrees_to_radians
         
    # Compute spherical distance from spherical coordinates.
         
    # For two locations in spherical coordinates 
    # (1, theta, phi) and (1, theta', phi')
    # cosine( arc length ) = 
    #    sin phi sin phi' cos(theta-theta') + cos phi cos phi'
    # distance = rho * arc length
     
    cos = (math.sin(phi1)*math.sin(phi2)*math.cos(theta1 - theta2) + 
           math.cos(phi1)*math.cos(phi2))
    arc = math.acos( cos )
 
    # Remember to multiply arc by the radius of the earth 
    # in your favorite set of units to get length.
    return arc

class Point(object):
    x = None
    y = None
    _id = None
    def __init__(self, x,y, _id):
        self.x = x
        self.y = y
        self._id = _id
    def x(self):
        return self.x
    def y(self):
        return self.y
    def __add__(self,other):
        return Point(self.x + other.x, self.y + other.y, 'add')
    def __div__(self,value):
        return Point(self.x/float(value),self.y/float(value), 'divide')
    def dist(self,other):
        return distance_on_unit_sphere(self.x, self.y, other.x, other.y)
    def closest(self,points):
        return min(points,key=self.dist)
    def __repr__(self):
        return "(%s,%f,%f)" % (self._id,self.x,self.y)

def update_centroids(points,centroids):
    groups = groupby(points,centroids)
    res = []
    for g in groups.values():
        res.append(sum(g, Point(0,0,'blah blah'))/len(g))
    return res

def groupby(points,centroids):
    g = defaultdict(list)
    for p in points:
        c = p.closest(centroids)
        g[c].append(p)
    return g

def run(xs, n, iters=15):
    centroids = xs[:n]
    for i in xrange(iters):
        centroids = update_centroids(xs,centroids)
    return groupby(xs,centroids)

if __name__ == "__main__":
    points = map(lambda x: Point(x['lat'],x['lng'], x['_id']), iter(collection.find()))
    iterations = 100

    res = None
    
    

    for i in xrange(iterations):
       res = run(points, 5)
    clusters.remove()
    for k in iter(res):
    	if len(res[k]) > 0:
    		cluser_id = clusters.save({'lat': k.x, 'lng': k.y, 'items': map(lambda x: x._id, iter(res[k]))})


    #for cluster in res:
    # 	cluser_id = clusters.insert_one({'lat': cluster[]}).inserted_id
    # 	for 
