(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;

(function () {

///////////////////////////////////////////////////////////////////////
//                                                                   //
// packages/kit:basic-auth/kit:basic-auth.js                         //
//                                                                   //
///////////////////////////////////////////////////////////////////////
                                                                     //
Meteor.startup(function () {                                         // 1
  var basicAuth = Npm.require('basic-auth-connect');                 // 2
  var auth = Meteor.settings.basicAuth || undefined;                 // 3
                                                                     // 4
  if (auth) {                                                        // 5
    WebApp.connectHandlers.use(basicAuth(function (user, pass) {     // 6
      return auth.username === user && auth.password === pass;       // 7
    }));                                                             // 8
                                                                     // 9
    // // move auth to the top of the stack                          // 10
    basicAuth = WebApp.connectHandlers.stack.pop();                  // 11
    WebApp.connectHandlers.stack.unshift(basicAuth);                 // 12
  }                                                                  // 13
});                                                                  // 14
///////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package['kit:basic-auth'] = {};

})();

//# sourceMappingURL=kit_basic-auth.js.map
