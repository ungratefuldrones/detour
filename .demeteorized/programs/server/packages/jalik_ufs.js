(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var _ = Package.underscore._;
var CollectionHooks = Package['matb33:collection-hooks'].CollectionHooks;
var MongoInternals = Package.mongo.MongoInternals;
var Mongo = Package.mongo.Mongo;

/* Package-scope variables */
var UploadFS;

(function () {

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                      //
// packages/jalik:ufs/ufs.js                                                                            //
//                                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                        //
var stores = {};                                                                                        // 1
                                                                                                        // 2
UploadFS = {                                                                                            // 3
    store: {},                                                                                          // 4
    /**                                                                                                 // 5
     * Returns the store by its name                                                                    // 6
     * @param name                                                                                      // 7
     * @return {UploadFS.Store}                                                                         // 8
     */                                                                                                 // 9
    getStore: function (name) {                                                                         // 10
        return stores[name];                                                                            // 11
    },                                                                                                  // 12
    /**                                                                                                 // 13
     * Returns all stores                                                                               // 14
     * @return {object}                                                                                 // 15
     */                                                                                                 // 16
    getStores: function () {                                                                            // 17
        return stores;                                                                                  // 18
    },                                                                                                  // 19
    /**                                                                                                 // 20
     * Returns file and data as ArrayBuffer for each files in the event                                 // 21
     * @param event                                                                                     // 22
     * @param callback                                                                                  // 23
     */                                                                                                 // 24
    readAsArrayBuffer: function (event, callback) {                                                     // 25
        // Check arguments                                                                              // 26
        if (typeof callback !== 'function') {                                                           // 27
            throw new TypeError('callback is not a function');                                          // 28
        }                                                                                               // 29
                                                                                                        // 30
        var reader = new FileReader();                                                                  // 31
        var files = event.target.files;                                                                 // 32
                                                                                                        // 33
        for (var i = 0; i < files.length; i += 1) {                                                     // 34
            var file = files[i];                                                                        // 35
                                                                                                        // 36
            (function (file) {                                                                          // 37
                reader.onloadend = function (ev) {                                                      // 38
                    callback.call(UploadFS, ev.target.result, file);                                    // 39
                };                                                                                      // 40
                reader.readAsArrayBuffer(file);                                                         // 41
            })(file);                                                                                   // 42
        }                                                                                               // 43
    }                                                                                                   // 44
};                                                                                                      // 45
                                                                                                        // 46
                                                                                                        // 47
if (Meteor.isServer) {                                                                                  // 48
    var Future = Npm.require('fibers/future');                                                          // 49
                                                                                                        // 50
    Meteor.methods({                                                                                    // 51
        /**                                                                                             // 52
         * Writes a chunk of file                                                                       // 53
         * @param chunk                                                                                 // 54
         * @param fileId                                                                                // 55
         * @param storeName                                                                             // 56
         * @return {*}                                                                                  // 57
         */                                                                                             // 58
        uploadChunkToStore: function (chunk, fileId, storeName) {                                       // 59
            check(fileId, String);                                                                      // 60
            check(storeName, String);                                                                   // 61
                                                                                                        // 62
            // Check arguments                                                                          // 63
            if (!(chunk instanceof Uint8Array)) {                                                       // 64
                throw new TypeError('chunk is not an Uint8Array');                                      // 65
            }                                                                                           // 66
            if (chunk.length <= 0) {                                                                    // 67
                throw new Error('chunk is empty');                                                      // 68
            }                                                                                           // 69
            if (!stores[storeName]) {                                                                   // 70
                throw new Error('store does not exist');                                                // 71
            }                                                                                           // 72
                                                                                                        // 73
            var fut = new Future();                                                                     // 74
            var store = stores[storeName];                                                              // 75
            var file = store.getCollection().findOne(fileId, {                                          // 76
                fields: {url: 1}                                                                        // 77
            });                                                                                         // 78
                                                                                                        // 79
            // Check that file exists                                                                   // 80
            if (!file) {                                                                                // 81
                throw new Error('file does not exist');                                                 // 82
            }                                                                                           // 83
                                                                                                        // 84
            // Check if URL is set                                                                      // 85
            if (!file.url) {                                                                            // 86
                store.getCollection().update(fileId, {                                                  // 87
                    $set: {url: store.getFileURL(fileId)}                                               // 88
                });                                                                                     // 89
            }                                                                                           // 90
                                                                                                        // 91
            // Write chunk to store                                                                     // 92
            store.write(chunk, fileId, function (err, length) {                                         // 93
                if (err) {                                                                              // 94
                    fut.throw(err);                                                                     // 95
                } else {                                                                                // 96
                    fut.return(length);                                                                 // 97
                }                                                                                       // 98
            });                                                                                         // 99
            return fut.wait();                                                                          // 100
        }                                                                                               // 101
    });                                                                                                 // 102
}                                                                                                       // 103
//////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                      //
// packages/jalik:ufs/ufs-store.js                                                                      //
//                                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                        //
/**                                                                                                     // 1
 * File store                                                                                           // 2
 * @param options                                                                                       // 3
 * @constructor                                                                                         // 4
 */                                                                                                     // 5
UploadFS.Store = function (options) {                                                                   // 6
    var self = this;                                                                                    // 7
                                                                                                        // 8
    // Set default options                                                                              // 9
    options = _.extend({                                                                                // 10
        collection: null,                                                                               // 11
        filter: null,                                                                                   // 12
        name: null                                                                                      // 13
    }, options);                                                                                        // 14
                                                                                                        // 15
    // Check instance                                                                                   // 16
    if (!(self instanceof UploadFS.Store)) {                                                            // 17
        throw new Error('UploadFS.Store is not an instance');                                           // 18
    }                                                                                                   // 19
                                                                                                        // 20
    // Check options                                                                                    // 21
    if (!(options.collection instanceof Mongo.Collection)) {                                            // 22
        throw new TypeError('collection is not a Mongo.Collection');                                    // 23
    }                                                                                                   // 24
    if (options.filter && !(options.filter instanceof UploadFS.Filter)) {                               // 25
        throw new TypeError('filter is not a UploadFS.Filter');                                         // 26
    }                                                                                                   // 27
    if (typeof options.name !== 'string') {                                                             // 28
        throw new TypeError('name is not a string');                                                    // 29
    }                                                                                                   // 30
    if (UploadFS.getStore(options.name)) {                                                              // 31
        throw new TypeError('name already exists');                                                     // 32
    }                                                                                                   // 33
                                                                                                        // 34
    // Private attributes                                                                               // 35
    var collection = options.collection;                                                                // 36
    var filter = options.filter;                                                                        // 37
    var name = options.name;                                                                            // 38
                                                                                                        // 39
    /**                                                                                                 // 40
     * Returns the collection                                                                           // 41
     * @return {Mongo.Collection}                                                                       // 42
     */                                                                                                 // 43
    self.getCollection = function () {                                                                  // 44
        return collection;                                                                              // 45
    };                                                                                                  // 46
                                                                                                        // 47
    /**                                                                                                 // 48
     * Returns the file filter                                                                          // 49
     * @return {UploadFS.Filter}                                                                        // 50
     */                                                                                                 // 51
    self.getFilter = function () {                                                                      // 52
        return filter;                                                                                  // 53
    };                                                                                                  // 54
                                                                                                        // 55
    /**                                                                                                 // 56
     * Returns the store name                                                                           // 57
     * @return {string}                                                                                 // 58
     */                                                                                                 // 59
    self.getName = function () {                                                                        // 60
        return name;                                                                                    // 61
    };                                                                                                  // 62
                                                                                                        // 63
    // Add the store to the list                                                                        // 64
    UploadFS.getStores()[name] = self;                                                                  // 65
                                                                                                        // 66
    // Add file information before insertion                                                            // 67
    collection.before.insert(function (userId, file) {                                                  // 68
        file.complete = false;                                                                          // 69
        file.uploading = false;                                                                         // 70
        file.store = name;                                                                              // 71
        file.extension = file.name.substr((~-file.name.lastIndexOf('.') >>> 0) + 2);                    // 72
        file.userId = userId;                                                                           // 73
    });                                                                                                 // 74
                                                                                                        // 75
    // Automatically delete file from store                                                             // 76
    // when the file is removed from the collection                                                     // 77
    collection.before.remove(function (userId, file) {                                                  // 78
        self.delete(file._id);                                                                          // 79
    });                                                                                                 // 80
                                                                                                        // 81
    // Set collection permissions                                                                       // 82
    collection.allow({                                                                                  // 83
        insert: function (userId, file) {                                                               // 84
            // Test file against filter                                                                 // 85
            filter && filter.check(file);                                                               // 86
            return typeof options.insert !== 'function'                                                 // 87
                || options.insert.apply(this, arguments);                                               // 88
        },                                                                                              // 89
        remove: function (userId, file) {                                                               // 90
            return typeof options.remove !== 'function'                                                 // 91
                || options.remove.apply(this, arguments);                                               // 92
        },                                                                                              // 93
        update: function (userId, doc, fieldNames, modifier) {                                          // 94
            return typeof options.update !== 'function'                                                 // 95
                || options.update.apply(this, arguments);                                               // 96
        }                                                                                               // 97
    });                                                                                                 // 98
};                                                                                                      // 99
                                                                                                        // 100
/**                                                                                                     // 101
 * Deletes a file async                                                                                 // 102
 * @param fileId                                                                                        // 103
 * @param callback                                                                                      // 104
 */                                                                                                     // 105
UploadFS.Store.prototype.delete = function (fileId, callback) {                                         // 106
    throw new Error('delete is not implemented');                                                       // 107
};                                                                                                      // 108
                                                                                                        // 109
/**                                                                                                     // 110
 * Returns the file URL                                                                                 // 111
 * @param fileId                                                                                        // 112
 */                                                                                                     // 113
UploadFS.Store.prototype.getFileURL = function (fileId) {                                               // 114
    throw new Error('getFileURL is not implemented');                                                   // 115
};                                                                                                      // 116
                                                                                                        // 117
/**                                                                                                     // 118
 * Reads a file async                                                                                   // 119
 * @param fileId                                                                                        // 120
 * @param callback                                                                                      // 121
 */                                                                                                     // 122
UploadFS.Store.prototype.read = function (fileId, callback) {                                           // 123
    throw new Error('read is not implemented');                                                         // 124
};                                                                                                      // 125
                                                                                                        // 126
/**                                                                                                     // 127
 * Writes a file chunk async                                                                            // 128
 * @param chunk                                                                                         // 129
 * @param fileId                                                                                        // 130
 * @param callback                                                                                      // 131
 */                                                                                                     // 132
UploadFS.Store.prototype.write = function (chunk, fileId, callback) {                                   // 133
    throw new Error('write is not implemented');                                                        // 134
};                                                                                                      // 135
//////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                      //
// packages/jalik:ufs/ufs-filter.js                                                                     //
//                                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                        //
/**                                                                                                     // 1
 * File filter                                                                                          // 2
 * @param options                                                                                       // 3
 * @constructor                                                                                         // 4
 */                                                                                                     // 5
UploadFS.Filter = function (options) {                                                                  // 6
    var self = this;                                                                                    // 7
                                                                                                        // 8
    // Set default options                                                                              // 9
    options = _.extend({                                                                                // 10
        contentTypes: null,                                                                             // 11
        extensions: null,                                                                               // 12
        maxSize: 1024 * 1000 * 10 // 10MB                                                               // 13
    }, options);                                                                                        // 14
                                                                                                        // 15
    // Check options                                                                                    // 16
    if (options.contentTypes && !(options.contentTypes instanceof Array)) {                             // 17
        throw new TypeError('contentTypes is not an Array');                                            // 18
    }                                                                                                   // 19
    if (options.extensions && !(options.extensions instanceof Array)) {                                 // 20
        throw new TypeError('extensions is not an Array');                                              // 21
    }                                                                                                   // 22
    if (typeof options.maxSize !== 'number') {                                                          // 23
        throw new TypeError('maxSize is not a number');                                                 // 24
    }                                                                                                   // 25
                                                                                                        // 26
    // Private attributes                                                                               // 27
    var contentTypes = options.contentTypes;                                                            // 28
    var extensions = options.extensions;                                                                // 29
    var maxSize = parseInt(options.maxSize);                                                            // 30
                                                                                                        // 31
    /**                                                                                                 // 32
     * Returns the allowed content types                                                                // 33
     * @return {Array}                                                                                  // 34
     */                                                                                                 // 35
    self.getContentTypes = function () {                                                                // 36
        return contentTypes;                                                                            // 37
    };                                                                                                  // 38
                                                                                                        // 39
    /**                                                                                                 // 40
     * Returns the allowed extensions                                                                   // 41
     * @return {Array}                                                                                  // 42
     */                                                                                                 // 43
    self.getExtensions = function () {                                                                  // 44
        return extensions;                                                                              // 45
    };                                                                                                  // 46
                                                                                                        // 47
    /**                                                                                                 // 48
     * Returns the max file size                                                                        // 49
     * @return {Number}                                                                                 // 50
     */                                                                                                 // 51
    self.getMaxSize = function () {                                                                     // 52
        return maxSize;                                                                                 // 53
    };                                                                                                  // 54
};                                                                                                      // 55
                                                                                                        // 56
                                                                                                        // 57
/**                                                                                                     // 58
 * Checks the file                                                                                      // 59
 * @param file                                                                                          // 60
 * @return {boolean}                                                                                    // 61
 */                                                                                                     // 62
UploadFS.Filter.prototype.check = function (file) {                                                     // 63
    // Check size                                                                                       // 64
    if (file.size > this.getMaxSize()) {                                                                // 65
        throw new Meteor.Error('file-too-large', 'The file is too large, max is ' + this.getMaxSize()); // 66
    }                                                                                                   // 67
    // Check extension                                                                                  // 68
    if (this.getExtensions() && !_.contains(this.getExtensions(), file.extension)) {                    // 69
        throw new Meteor.Error('invalid-file-extension', 'The file extension is not accepted');         // 70
    }                                                                                                   // 71
    // Check content type                                                                               // 72
    if (this.getContentTypes() && checkContentType(file.extension, this.getContentTypes())) {           // 73
        throw new Meteor.Error('invalid-file-type', 'The file type is not accepted');                   // 74
    }                                                                                                   // 75
    return true;                                                                                        // 76
};                                                                                                      // 77
                                                                                                        // 78
function checkContentType(type, types) {                                                                // 79
    type = type.toLowerCase();                                                                          // 80
                                                                                                        // 81
    for (var i = 0; i < types.length; i += 1) {                                                         // 82
        var pattern = types[i].toLowerCase();                                                           // 83
                                                                                                        // 84
        if (pattern.indexOf('*') !== -1) {                                                              // 85
            return type.indexOf(pattern.replace('*', '')) === 0;                                        // 86
                                                                                                        // 87
        } else if (pattern === type) {                                                                  // 88
            return true;                                                                                // 89
        }                                                                                               // 90
    }                                                                                                   // 91
    return false;                                                                                       // 92
}                                                                                                       // 93
//////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package['jalik:ufs'] = {
  UploadFS: UploadFS
};

})();

//# sourceMappingURL=jalik_ufs.js.map
