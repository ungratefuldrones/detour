(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var _ = Package.underscore._;
var ReactiveVar = Package['reactive-var'].ReactiveVar;

/* Package-scope variables */
var JWPlayer;



/* Exports */
if (typeof Package === 'undefined') Package = {};
Package['jeremy:jwplayer'] = {
  JWPlayer: JWPlayer
};

})();

//# sourceMappingURL=jeremy_jwplayer.js.map
