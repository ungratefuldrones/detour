(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var WebApp = Package.webapp.WebApp;
var main = Package.webapp.main;
var WebAppInternals = Package.webapp.WebAppInternals;
var UploadFS = Package['jalik:ufs'].UploadFS;

(function () {

/////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                 //
// packages/jalik:ufs-local/ufs-local.js                                                           //
//                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                   //
if (Meteor.isServer) {                                                                             // 1
    var fs = Npm.require('fs');                                                                    // 2
    var mkdirp = Npm.require('mkdirp');                                                            // 3
}                                                                                                  // 4
                                                                                                   // 5
/**                                                                                                // 6
 * File system store                                                                               // 7
 * @param options                                                                                  // 8
 * @constructor                                                                                    // 9
 */                                                                                                // 10
UploadFS.store.Local = function (options) {                                                        // 11
    // Set default options                                                                         // 12
    options = _.extend({                                                                           // 13
        path: 'uploads'                                                                            // 14
    }, options);                                                                                   // 15
                                                                                                   // 16
    // Check options                                                                               // 17
    if (typeof options.path !== 'string') {                                                        // 18
        throw new TypeError('path is not a string');                                               // 19
    }                                                                                              // 20
                                                                                                   // 21
    // Private attributes                                                                          // 22
    var path = options.path;                                                                       // 23
                                                                                                   // 24
    // Create the upload dir                                                                       // 25
    if (Meteor.isServer) {                                                                         // 26
        mkdirp(path, function (err) {                                                              // 27
            if (err) {                                                                             // 28
                console.error('error creating store path ' + path);                                // 29
            } else {                                                                               // 30
                console.info('created store path ' + path);                                        // 31
            }                                                                                      // 32
        });                                                                                        // 33
    }                                                                                              // 34
                                                                                                   // 35
    // Create the store                                                                            // 36
    var store = new UploadFS.Store(options);                                                       // 37
                                                                                                   // 38
    /**                                                                                            // 39
     * Removes the file                                                                            // 40
     * @param fileId                                                                               // 41
     * @param callback                                                                             // 42
     */                                                                                            // 43
    store.delete = function (fileId, callback) {                                                   // 44
        var path = store.getFilePath(fileId);                                                      // 45
                                                                                                   // 46
        if (typeof callback !== 'function') {                                                      // 47
            callback = function (err) {                                                            // 48
                if (err) {                                                                         // 49
                    console.error(err);                                                            // 50
                }                                                                                  // 51
            }                                                                                      // 52
        }                                                                                          // 53
        fs.unlink(path, callback);                                                                 // 54
    };                                                                                             // 55
                                                                                                   // 56
    /**                                                                                            // 57
     * Returns the file path                                                                       // 58
     * @param fileId                                                                               // 59
     * @return {string}                                                                            // 60
     */                                                                                            // 61
    store.getFilePath = function (fileId) {                                                        // 62
        var file = store.getCollection().findOne(fileId, {                                         // 63
            fields: {extension: 1}                                                                 // 64
        });                                                                                        // 65
        return store.getPath() + '/' + fileId + '.' + file.extension;                              // 66
    };                                                                                             // 67
                                                                                                   // 68
    /**                                                                                            // 69
     * Returns the file URL                                                                        // 70
     * @param fileId                                                                               // 71
     * @return {string}                                                                            // 72
     */                                                                                            // 73
    store.getFileURL = function (fileId) {                                                         // 74
        var file = store.getCollection().findOne(fileId, {                                         // 75
            fields: {extension: 1}                                                                 // 76
        });                                                                                        // 77
        return Meteor.absoluteUrl('ufs/' + store.getName() + '/' + fileId + '.' + file.extension); // 78
    };                                                                                             // 79
                                                                                                   // 80
    /**                                                                                            // 81
     * Returns the path where files are saved                                                      // 82
     * @return {string}                                                                            // 83
     */                                                                                            // 84
    store.getPath = function () {                                                                  // 85
        return path;                                                                               // 86
    };                                                                                             // 87
                                                                                                   // 88
    /**                                                                                            // 89
     * Returns the file read stream                                                                // 90
     * @param fileId                                                                               // 91
     * @return {*}                                                                                 // 92
     */                                                                                            // 93
    store.read = function (fileId) {                                                               // 94
        var path = store.getFilePath(fileId);                                                      // 95
        return fs.createReadStream(path);                                                          // 96
    };                                                                                             // 97
                                                                                                   // 98
    /**                                                                                            // 99
     * Writes the chunk to file system                                                             // 100
     * @param chunk                                                                                // 101
     * @param fileId                                                                               // 102
     * @param callback                                                                             // 103
     */                                                                                            // 104
    store.write = function (chunk, fileId, callback) {                                             // 105
        var path = store.getFilePath(fileId);                                                      // 106
                                                                                                   // 107
        fs.appendFile(path, new Buffer(chunk), function (err) {                                    // 108
            if (err) {                                                                             // 109
                callback.call(store, err);                                                         // 110
            } else {                                                                               // 111
                callback.call(store, null, chunk.length);                                          // 112
            }                                                                                      // 113
        });                                                                                        // 114
    };                                                                                             // 115
                                                                                                   // 116
    return store;                                                                                  // 117
};                                                                                                 // 118
                                                                                                   // 119
                                                                                                   // 120
if (Meteor.isServer) {                                                                             // 121
    var zlib = Npm.require('zlib');                                                                // 122
                                                                                                   // 123
    // Listen HTTP requests to serve files                                                         // 124
    WebApp.connectHandlers.use(function (req, res, next) {                                         // 125
        // Check if request matches /ufs/store/file.ext                                            // 126
        var match = /^\/ufs\/([^\/]+)\/([^\/]+)$/.exec(req.url);                                   // 127
                                                                                                   // 128
        if (match !== null) {                                                                      // 129
            // Get store                                                                           // 130
            var storeName = match[1];                                                              // 131
            var store = UploadFS.getStore(storeName);                                              // 132
            if (!store) {                                                                          // 133
                res.writeHead(404, {});                                                            // 134
                res.end();                                                                         // 135
                return;                                                                            // 136
            }                                                                                      // 137
                                                                                                   // 138
            // Get file from database                                                              // 139
            var fileId = match[2].replace(/\.[^.]+$/, '');                                         // 140
            var file = store.getCollection().findOne(fileId);                                      // 141
            if (!file) {                                                                           // 142
                res.writeHead(404, {});                                                            // 143
                res.end();                                                                         // 144
                return;                                                                            // 145
            }                                                                                      // 146
                                                                                                   // 147
            // Get file data from path                                                             // 148
            var rs = store.read(fileId);                                                           // 149
            var acceptEncoding = req.headers['accept-encoding'];                                   // 150
                                                                                                   // 151
            if (!acceptEncoding) {                                                                 // 152
                acceptEncoding = '';                                                               // 153
            }                                                                                      // 154
                                                                                                   // 155
            // Compress data if supported by the client                                            // 156
            if (acceptEncoding.match(/\bdeflate\b/)) {                                             // 157
                res.writeHead(200, {                                                               // 158
                    'Content-Encoding': 'deflate',                                                 // 159
                    'Content-Type': file.type                                                      // 160
                });                                                                                // 161
                rs.pipe(zlib.createDeflate()).pipe(res);                                           // 162
                                                                                                   // 163
            } else if (acceptEncoding.match(/\bgzip\b/)) {                                         // 164
                res.writeHead(200, {                                                               // 165
                    'Content-Encoding': 'gzip',                                                    // 166
                    'Content-Type': file.type                                                      // 167
                });                                                                                // 168
                rs.pipe(zlib.createGzip()).pipe(res);                                              // 169
                                                                                                   // 170
            } else {                                                                               // 171
                res.writeHead(200, {});                                                            // 172
                rs.pipe(res);                                                                      // 173
            }                                                                                      // 174
                                                                                                   // 175
        } else {                                                                                   // 176
            next();                                                                                // 177
        }                                                                                          // 178
    });                                                                                            // 179
}                                                                                                  // 180
/////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package['jalik:ufs-local'] = {};

})();

//# sourceMappingURL=jalik_ufs-local.js.map
