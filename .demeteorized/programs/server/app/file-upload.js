(function(){/* routes */

Router.route('/', function () {
  this.render('Home');
});

Router.route('/items/:_id', function () {
  var item = events.findOne({_id: this.params._id});
  item.single = true;
  this.render('singleItem', {data: item});
});

var MAP_ZOOM = 18;




//var handle = Meteor.subscribeWithPagination('events', 2);
/*
Meteor.publish('events', function(limit) {
  return events.find({}, {limit: limit, sort :{createdAt: -1}});
});
*/
FS.debug = true;

// Scrolling
// 
// https://gentlenode.com/journal/meteor-21-pagination-system-with-infinite-scroll/43


// don't show until upload complete
// https://github.com/CollectionFS/Meteor-CollectionFS/issues/277
// http://artandlogic.com/2014/03/recording-audio-video-with-html5-co-starring-meteor/
// https://github.com/CollectionFS/Meteor-cfs-ui#fsuploadprogressbar
// http://www.manuel-schoebel.com/blog/meteor-and-seo
// http://stackoverflow.com/questions/29490110/how-to-add-more-files-at-once-using-fs-collection-in-meteor-js
// http://meteortuts.com/
// https://github.com/CollectionFS/Meteor-cfs-ui
//http://geocoder.opencagedata.com/


var eventPhotosStore = new FS.Store.FileSystem('eventPhotos', {
    path: '~/uploads/new'
});




eventPhotos = new FS.Collection('eventPhotos', {
    stores: [eventPhotosStore]
});

/*eventPhotos.on("stored", function (fileObj, storeName) {
    debugger;
    var url = fileObj.url({store: storeName});
});
*/

eventPhotos.allow({
    insert: function() {
        return true;
    },
    update: function() {
        return true;
    },
    remove: function() {
        return true;
    },
    download: function() {
        return true;
    }
});




events = new Meteor.Collection('assets');

if (Meteor.isClient) {
    Meteor.startup(function() {  
      GoogleMaps.load();
    });
    Geolocation.currentLocation();
    JWPlayer.load('rgMoznb2EeONhCIACmOLpg');

    Template.hello.events({
        'submit form#story': function() {
            event.preventDefault();
            var timestamp = +new Date();
            var file = $('#file').get(0).files[0];
            var fsFile = new FS.File(file);
            fsFile.name(String(timestamp) + fsFile.name());
            var fileObj = eventPhotos.insert(fsFile, function (err) {
              if (err) throw err;
            });
            var body = $('#body_content').get(0).value;
            var name = $('#name_content').get(0).value;
            //var fileObj = eventPhotos.insert(file);
            var geo = Geolocation.latLng();
            console.log('Upload result: ', fileObj);

            events.insert({
                name: name,
                body: body,
                file: fileObj,
                file_name: fsFile.name(),
                lat: geo['lat'],
                lng: geo['lng'],
                createdAt: new Date()
            });


        }
    });

    Template.imageView.helpers({
        events: function() {
            var blah = events.find({}, {
                sort: {
                    createdAt: -1
                }
            });
            return blah;
        }
    });

    Template.singleView.helpers({
        image: function() {
            var blah = eventPhotos.findOne({
                "original.name": this.file_name
            });
            return blah;
        }

    });

    Template.singleView.events({
        "click .delete": function () {
            var blah = eventPhotos.findOne({
                "original.name": this.file_name
            });
            try{
                eventPhotos.remove(blah._id);
            } catch(err) {
                console.log(err);
            }
          events.remove(this._id);
        }   
    });



    Template.imageView.events({
        'click #load-more': function(){
            //handle.loadNextPage();
        }
    });

    Template.map.helpers({  
      geolocationError: function() {
        var error = Geolocation.error();
        return error && error.message;
      },
      mapName: function(){
        return 'map-' + this._id;
      },
      mapOptions: function() {

       var latLng = Geolocation.latLng();
        // Initialize the map once we have the latLng.
        if (GoogleMaps.loaded() && this.lat && this.lng) {
          return {
            center: new google.maps.LatLng(this.lat, this.lng),
            zoom: MAP_ZOOM
          };
        }
      }
    });

    Template.map.onCreated(function() {
      // We can use the `ready` callback to interact with the map API once the map is ready.
      GoogleMaps.ready('map', function(map) {
        // Add a marker to the map once it's ready
        var marker = new google.maps.Marker({
          position: map.options.center,
          map: map.instance
        });
      });
    });




    Template.hello.created = function() {
        //
    };

    Template.audioTemplate.onRendered(function() {
        
            var that = this;
            this.autorun(function() {
                    if (JWPlayer.loaded()) {
                        //var wait = Meteor.setInterval(function() {
                            //http request
                                //debugger;
                                //console.dir(that);

                                console.log('/cfs/files/eventPhotos/' + that.data._id + '/' + that.data.original.name);
                                jwplayer('audio-' + that.data._id ).setup({
                                    file: '/cfs/files/eventPhotos/' + that.data._id + '/' + that.data.original.name,
                                    width: '100%',
                                    height: '40px'
                                    
                                });

                               // Meteor.clearInterval(wait);

                          
                       // }, 300);


                    }
            });

    });

    Template.videoTemplate.onRendered(function() {
        
            var that = this;
            this.autorun(function() {
                    if (JWPlayer.loaded()) {
                        //var wait = Meteor.setInterval(function() {
                            //http request
                                //debugger;
                                //console.dir(that);

                                console.log('/cfs/files/eventPhotos/' + that.data._id + '/' + that.data.original.name);
                                jwplayer('audio-' + that.data._id ).setup({
                                    file: '/cfs/files/eventPhotos/' + that.data._id + '/' + that.data.original.name,
                                    width: '100%',
                                    aspectratio: "16:9"
                                    
                                });

                               // Meteor.clearInterval(wait);

                          
                       // }, 300);


                    }
            });

    });



}




 

})();
