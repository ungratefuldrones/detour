if (Meteor.isClient) {


    $.cloudinary.config({cloud_name:"dowcdhkuo"});
      $('body').on('touchmove', function (e) {
      if (! $('.scrollable').has($(e.target)).length)
        e.preventDefault();
    });

    function padWithZero(input, length) {
    // Cast input to string
    input = "" + input;

    var paddingSize = Math.max(0, length - input.length);
    return new Array(paddingSize > 0 ? paddingSize + 1 : 0).join("0") + input;
}

    prod = window.location.origin.indexOf('local') === -1;
    media_root = prod ? "http://45.55.44.97/uploads/" : 'http://media.local:8888/';
    bonusMode = {
        mode: 'none',
        dep: new Deps.Dependency, //save dependent computations here
        get: function() {
            this.dep.depend(); //saves the Deps.currentComputation
            return this.mode;
        },
        set: function(newValue) {
            this.mode = newValue;
            this.dep.changed(); //invalidates all dependent computations
            return this.mode;
        }
    };

    radius = {
        m: 1000,
        dep: new Deps.Dependency, //save dependent computations here
        get: function() {
            this.dep.depend(); //saves the Deps.currentComputation
            return this.m;
        },
        set: function(newValue) {
            this.m = newValue;
            this.dep.changed(); //invalidates all dependent computations
            return this.m;
        }
    };
   
    commentMode = {
        mode: 'none',
        dep: new Deps.Dependency, //save dependent computations here
        get: function() {
            this.dep.depend(); //saves the Deps.currentComputation
            return this.mode;
        },
        set: function(newValue) {
            this.mode = newValue;
            this.dep.changed(); //invalidates all dependent computations
            return this.mode;
        }
    };

    audioState = {
        mode: 'none',
        dep: new Deps.Dependency, //save dependent computations here
        get: function() {
            this.dep.depend(); //saves the Deps.currentComputation
            return this.mode;
        },
        set: function(newValue) {
            this.mode = newValue;
            this.dep.changed(); //invalidates all dependent computations
            return this.mode;
        }
    };


    multimediaController = {
        file: 'none',
        last: null,
        dep: new Deps.Dependency, //save dependent computations here
        get: function() {
            this.dep.depend(); //saves the Deps.currentComputation
            return this.file;
        },
        set: function(newValue) {
            if(this.file == newValue){return;}
            this.file = newValue;
            this.dep.changed(); //invalidates all dependent computations
            if ( window.player ){
              window.player.unload();
              window.player = null;
            }
            
            window.player =  new Howl({src:media_root + escape(newValue), autoplay: true, html5:true})
            window.player.on('play', function() {
              audioState.set('play');
              window.player.time_total = padWithZero(moment.duration(window.player.duration()* 1000).minutes(), 2) + ':' + padWithZero(moment.duration(window.player.duration()* 1000).seconds(), 2);
              window.player_update = Meteor.setInterval(function() {
                $("footer .playbar-overlay").css("width", window.player.seek() / window.player.duration() * 100 + "%");
                var current = moment.duration(window.player.seek()* 1000);
                $("footer .timecode").text(padWithZero(current.minutes(),2) + ':' + padWithZero(current.seconds(),2) + '/' + window.player.time_total);
              }, 500);
             });
            window.player.on('end', function(){
              Meteor.clearInterval(window.player_update);  
              audioState.set('stop');

              if (this._src.indexOf(multimediaController.get()) > -1){
                setTimeout(function(){
                    multimediaController.set('none');
                    window.player.unload();
                  
                }, 1000);
              }
            });
    
            return this.file;
        }
    };

    Meteor.startup(function() {
        if (!Session.get('radius')){
          Session.setPersistent('radius', 500);
        }
        //Mapbox.load();

        Meteor.setInterval(function() {
            navigator.geolocation.getCurrentPosition(function(position) {
                Session.setPersistent('lat', position.coords.latitude);
                Session.setPersistent('lon', position.coords.longitude);
            });
        }, 5000);

        navigator.geolocation.getCurrentPosition(function(position) {
              Session.setPersistent('lat', position.coords.latitude);
              Session.setPersistent('lon', position.coords.longitude);
          });


        multimediaController.set('none');
        // console.log(Meteor.default_connection._lastSessionId);

    });

    
    Deps.autorun(function() {


    });

    JWPlayer.load('rgMoznb2EeONhCIACmOLpg');

}
