Template.imageView.helpers({
});

Template.imageView.events({
    'click #load-more': function() {
        //handle.loadNextPage();
    },

});

Template.singleView.helpers({
    image: function() {
        var blah = eventPhotos.findOne({
            "original.name": this.file_name
        });
        if (blah){
          blah.parent_url = '/items/' + this._id;
        }
        return blah;
    },
    isActive: function() {
        return bonusMode.get() == this._id;
    },
    neighborhood: function(){
        return this.address.neighbourhood || this.address.suburb || this.address.town || this.address.village || this.address.hamlet || this.address.city;
    },
    commentButton: function(){
        return commentMode.get() != this._id;
    }, 
    type: function(){
        var blah = eventPhotos.findOne({
            "original.name": this.file_name
        });
         
        if (typeof this.type != 'undefined' && this.type.length ){
            return this.type;
        } else if (typeof blah !== 'undefined') {
            if (blah.original.type.indexOf('audio') > -1 ){
                return 'audio';
            } else if (blah.original.type.indexOf('image') > -1 ){
                return 'image';
            } else if (blah.original.type.indexOf('video') > -1 ){
                return 'video';
            }
        } else {
            return 'story';
        }
    }
    ,bodyEdit: function(){
      return '<div class="editable" contentEditable="true" tabindex=0>' + this.body + '</div>';
    }
    ,byline: function(){
        return 'anonymous';
    }
    ,nearby: function(){
        //debugger;
        var to_return = events.find({
                        loc: {
                            $near: {
                                $geometry: {
                                    type: "Point",
                                    coordinates: [this.lng, this.lat]
                                },
                                $minDistance: 0,
                                $maxDistance: 10000
                            }
                        },
                        _id:{
                             $nin: [this._id]
                        }
                    }, {
                        limit : 5
                        // ,sort: {
                        //     createdAt: -1
                        // } 
                    });

        return to_return;
    }
    ,hasBody: function(){
        return typeof this.body === 'undefined' || this.body.length == 0;
    }
});

Template.singleView.events({
    "click .delete": function() {
        var blah = eventPhotos.findOne({
            "original.name": this.file_name
        });
        try {
            eventPhotos.remove(blah._id);
        } catch (err) {
            console.log(err);
        }
        events.remove(this._id);
    },
    "click .edit": function() {
        bonusMode.set(this._id);
        history.pushState({
            _id: this._id
        }, "", "/items/" + this._id);
        window.scrollTo(0, $('#' + this._id).offset()[1]);

    },
    "click .add-comment": function(event, template){
        commentMode.set(this._id);
        _.defer(function(){
            $("#comment_body").focus();
        });
    },
    "submit #comment-form": function(event, template){
        event.preventDefault();
        var body = $('#comment_body').get(0).value;
        if (typeof body !== 'undefined' && body.length > 0){
          var to_insert = {
              createdAt: new Date(),
              body: body
          };
    
        events.update({_id: commentMode.get()}, {$push: {comments: to_insert}});
        commentMode.set('none');
    }
  }
  ,"blur .editable": function(event, template){
        //debugger;
        //event.preventDefault(); 
        //event.stopImmediatePropagation();
        var source = $(event.currentTarget)[0].innerHTML;
        var source_safe = $(event.currentTarget)[0].innerText;
        events.update(this._id, {$set :{ body: source, body_safe: source_safe}});

    }
});

// Template.singleView.onRendered(function(){
//     $('a[rel="lightbox"]').fluidbox();
// });

