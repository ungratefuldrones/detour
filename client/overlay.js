function truncText(text, maxLength, ellipseText) {
  ellipseText = ellipseText || '...';

  if (text.length < maxLength)
      return text;

  //Find the last piece of string that contain a series of not A-Za-z0-9_ followed by A-Za-z0-9_ starting from maxLength
  var m = text.substr(0, maxLength).match(/([^A-Za-z0-9_]*)[A-Za-z0-9_]*$/);
  if (!m) return ellipseText;

  //Position of last output character
  var lastCharPosition = maxLength - m[0].length;

  //If it is a space or "[" or "(" or "{" then stop one before. 
  if (/[\s\(\[\{]/.test(text[lastCharPosition])) lastCharPosition--;

  //Make sure we do not just return a letter..
  return (lastCharPosition ? text.substr(0, lastCharPosition + 1) : '') + ellipseText;
}

Template.overlay.events({
  "click a.grid-item": function() {
      Router.go('item.single', {_id: this._id});
      $("#overlay").scrollTop(0);
    }
    ,"click .delete": function() {
        var blah = eventPhotos.findOne({
            "original.name": this.file_name
        });
        try {
            eventPhotos.remove(blah._id);
        } catch (err) {
            console.log(err);
        }
        events.remove(this._id);
    }

});

Template.overlay.helpers({
  image: function() {
      return eventPhotos.findOne({
          "original.name": this.file_name
      });
  },
  isCluster: function() {
      return typeof bonusMode.get().cluster !== 'undefined' || ['nearby', 'recent', 'trending', 'bugs'].indexOf(bonusMode.get()) > -1;
  },
  teaser: function() {
      if (typeof this.body_safe !== 'undefined'){
        return truncText(this.body_safe, 60);
      } else {
        return '';
      }
  }
});


Template.overlay.onRendered(function(){
});
