Template.registerHelper('formatDate', function(date) {
  return moment(this.createdAt).toNow(true) + ' ago';
});

Template.registerHelper('safeTitle', function(text){
	return text.length > 0 ? text : "Untitled";
});

Template.registerHelper('thumbURL', function(image){;
	return media_root + 'thumbs/' + this.copies.thumbs.key;
});

Template.registerHelper('mediumURL', function(image){
	return media_root  + 'medium/' + this.copies.medium.key; 
});

Template.registerHelper('imgURL', function(image){
	return media_root  + this.copies.eventPhotos.key;
});

Template.registerHelper('distance', function(lat1, lon1, lat2, lon2) {
  var p = 0.017453292519943295;    // Math.PI / 180
  var c = Math.cos;
  var a = 0.5 - c((lat2 - lat1) * p)/2 + 
          c(lat1 * p) * c(lat2 * p) * 
          (1 - c((lon2 - lon1) * p))/2;

  return Math.round(12742000 * Math.asin(Math.sqrt(a))); // 2 * R; R = 6371 km)) // multiplied by 1000 and returns metres
});

Template.registerHelper('_lat', function(){
	return Session.get('lat');
});

Template.registerHelper('_lng', function(){
	return Session.get('lon');
});