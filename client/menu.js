Template.menu.helpers({
	backHome: function(){
		return ['none', "hud"].indexOf(bonusMode.get()) === -1;
	}, current: function(){
		var temp = bonusMode.get();
		if (temp == 'add'){
			return "YOUR STORY";
		} else if(typeof temp.cluster !== 'undefined' ){
			return "NEAR " + temp.neighborhood.toUpperCase();
		} else {
			return ['nearby','trending', 'recent', 'options'].indexOf(temp) > -1 ? temp.toUpperCase() : ''; 
		}
	}
});

Template.menu.events({
	"click .back": function(event, template){
		event.preventDefault();
		history.back();
	},"click .add": function(event, template) {
        event.preventDefault();
        Router.go('/add');

    },"click .main": function(event, template){
    	if( bonusMode.get() == 'none'){
    		bonusMode.set('hud');
    	}
    }
    // ,"mouseover .main":function(event, template){
    // 	$(event.currentTarget).text('Return to map');
    // }

});