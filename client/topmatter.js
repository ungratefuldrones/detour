Template.vote.events({
    "click .up": function(event, template){
        event.preventDefault();
        event.stopPropagation();
        events.update({_id: this._id}, {$inc:{"votes":1}});
    },
    "click .down": function(event, template){
        event.preventDefault();
        event.stopPropagation();
        events.update({_id: this._id}, {$inc:{"votes":-1}});
    }
});

Template.vote.helpers({
  votes: function(){
      return this.votes || 0; 
  }

});