Template.hud.helpers({
	nearby_items: function(){
		return events.find({
              loc: {
                  $near: {
                      $geometry: {
                          type: "Point",
                          coordinates: [Session.get('lon'), Session.get('lat')]
                      },
                      $minDistance: 0,
                      $maxDistance: parseInt(radius.get())
                  }
              },
                type:{
                     $nin: ['bug', 'feature']
                }
          }, {
              // sort: {
              //     createdAt: -1
              // },
              limit: 4
      });
	},
	recent_items: function(){
		return events.find({
                        type:{
                             $nin: ['bug', 'feature']
                        }}, {
              sort: {
                  createdAt: -1
              }, limit : 4
          });
	},
	trending_items: function(){
		return events.find({
                            type:{
                                 $nin: ['bug', 'feature']
                            }, votes: {$gt: 0}}, {
              sort: {
              	  votes: -1,
                  createdAt: -1
              }, limit : 4
          });
	},
	image: function() {

        var blah = eventPhotos.findOne({
            "original.name": this.file_name
        });
        if (blah){
          blah.parent_url = '/items/' + this._id;
        }
        return blah;
    }

});

Template.hud.events({
	"click .explore": function(event, template){
		event.preventDefault();
		bonusMode.set('none');
    Session.set('hud_disable', true);
	},
  "click .configure": function(event, template){
    event.preventDefault();
    Router.go('/options');
  }
  // ,"click #nearby_items": function(event, template){
  //   event.preventDefault();
  //   event.stopPropagation();
  //   Router.go('/nearby');
  // },
  // "click #recent_items": function(event, template){
  //   event.preventDefault();
  //   event.stopPropagation();
  //   Router.go('/recent');
  // },
  // "click #trending_items": function(event, template){
  //   if(event.currentTarget == event.target){
  //     event.preventDefault();
  //     event.stopPropagation();
  //     Router.go('/trending');
  //   }
  // },
  // "click .top-links": function(event, template){
    
  //     //event.preventDefault();
  //     event.stopPropagation();
    
  // }
});


