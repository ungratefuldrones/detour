Router.route('/items',  {
    waitOn: function() {
        bonusMode.set('none');
        return;
        //return [Meteor.subscribe('events',3)]       
      },
    template : 'Home',
    name: 'item.list'
});

Router.route('/add1',  {
    waitOn: function() {
        bonusMode.set('none');
        return;
        //return [Meteor.subscribe('events',3)]       
      },
    template : 'fileUpload',
    name: 'add1'
});

Router.route('/mapbox',  {
    waitOn: function() {
        Mapbox.debug = true;
        //return [IRLibLoader.load('https://api.tiles.mapbox.com/mapbox.js/v2.1.5/mapbox.js')];
        Mapbox.load();
        return;
        //return;
        //return [Meteor.subscribe('events',3)]       
      },
    // onBeforeAction: function(){
    //   var one = IRLibLoader.load('https://api.tiles.mapbox.com/mapbox.js/v2.1.5/mapbox.js', {
    //     success: function(){ console.log('SUCCESS CALLBACK'); },
    //     error: function(){ console.log('ERROR CALLBACK'); }
    //   });
    //   if(one.ready()){

    //     var two = IRLibLoader.load('https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/leaflet.markercluster.js');
    //     if(two.ready()){
    //       this.next();
    //     }
    //   }
    // },
    template : 'box',
    name: 'box'
});

Router.route('/add', {
    waitOn: function(){
        bonusMode.set('add')
        return [IRLibLoader.load('http://bl.ocks.org/mbostock/raw/5616813/d3.geo.tile.min.js')]
    },
    template: 'reactivemap'
});

Router.route('/options', {
    waitOn: function(){
        bonusMode.set('options')
        return [IRLibLoader.load('http://bl.ocks.org/mbostock/raw/5616813/d3.geo.tile.min.js')]
    },
    template: 'reactivemap'
});

Router.route('/', {
    waitOn: function(){
        bonusMode.set('hud')
        return [IRLibLoader.load('http://bl.ocks.org/mbostock/raw/5616813/d3.geo.tile.min.js')]
    },
    template: 'reactivemap',
    name: "home"
});
/*
Router.route('/items/:_id', function () {
  var item = events.findOne({_id: this.params._id});
  item.single = true;
  bonusMode.set(item._id);
  this.render('Home', {data: item});

});
*/
Router.route('/help', {
    waitOn: function(){
        bonusMode.set('help')
        return [IRLibLoader.load('http://bl.ocks.org/mbostock/raw/5616813/d3.geo.tile.min.js')]
    },
    template: 'reactivemap',
    name: 'help'
});

Router.route('/items/:_id', {
    waitOn: function(){
        bonusMode.set({item: this.params._id});
        $("#overlay").animate({scrollTop: 0}, 250);
        return [IRLibLoader.load('http://bl.ocks.org/mbostock/raw/5616813/d3.geo.tile.min.js')
                // ,IRLibLoader.load("http://davidecalignano.it/project/files/medium-lightbox/demo/medium-lightbox.js")
                // ,IRLibLoader.load("http://davidecalignano.it/project/files/medium-lightbox/demo/style.css")
                //,IRLibLoader.load("https://cdnjs.cloudflare.com/ajax/libs/fluidbox/1.4.3.1/css/fluidbox.css")
            ]
    },
    template: 'reactivemap',
    name: "item.single",
    data: function(){
      var to_return = {
            item: events.find({_id: this.params._id})
        };
        //console.log(to_return.item.fetch());
        return to_return;
    }

});

Router.route('/cluster/:_id', {
    waitOn: function(){
        return [IRLibLoader.load('http://bl.ocks.org/mbostock/raw/5616813/d3.geo.tile.min.js')]
    },
    template: 'reactivemap',
    name: "cluster",
    data: function(){
        //debugger;
        cluster = clusters.findOne({"properties.FIPS": this.params._id});

         if (cluster){
        //     var to_return = {
        //         item: events.find({
        //                     loc: {
        //                         $near: {
        //                             $geometry: {
        //                                 type: "Point",
        //                                 coordinates: cluster.geometry.coordinates
        //                             },
        //                             $minDistance: 0,
        //                             $maxDistance: cluster.properties.radius
        //                         }
        //                     },
        //                     type:{
        //                          $nin: ['bug', 'feature']
        //                     }
        //                 }, {
        //                     sort: {
        //                         createdAt: -1
        //                     }
        //                 })
        //     };

            var to_return = { item: events.find({cluster: this.params._id},{
                            sort: {
                                createdAt: -1
                            }
                        }) };

            bonusMode.set({cluster: this.params._id, neighborhood:cluster.properties.address.neighbourhood || cluster.properties.address.suburb || cluster.properties.address.town || cluster.properties.address.village || cluster.properties.address.city});
           //            bonusMode.set({cluster: this.params._id, neighborhood: (cluster.properties.address.road || '') + (cluster.properties.address.neighbourhood || cluster.properties.address.suburb || cluster.properties.address.town || cluster.properties.address.village || cluster.properties.address.city)});


            //console.log(to_return.item.fetch());
            return to_return;
        }
    }

});


Router.route('/nearby', {
    waitOn: function(){
        bonusMode.set('nearby');
        return [IRLibLoader.load('http://bl.ocks.org/mbostock/raw/5616813/d3.geo.tile.min.js')]
    },
    template: 'reactivemap',
    name: "nearby",
    data: function(){
            var to_return = {
                item: events.find({
                            loc: {
                                $near: {
                                    $geometry: {
                                        type: "Point",
                                        coordinates: [Session.get('lon'), Session.get('lat')]
                                    },
                                    $minDistance: 0,
                                    $maxDistance: parseInt(radius.get())
                                }
                            },
                            type:{
                                 $nin: ['bug', 'feature']
                            }
                        }, {
                            // sort: {
                            //     createdAt: -1
                            // }
                        })
            };
            //console.log(to_return.item.fetch());
            return to_return;
    }

});

Router.route('/trending', {
    waitOn: function(){
        bonusMode.set('trending');
        return [IRLibLoader.load('http://bl.ocks.org/mbostock/raw/5616813/d3.geo.tile.min.js')]
    },
    template: 'reactivemap',
    name: "trending",
    data: function(){
            var to_return = {
                item: events.find({
                            type:{
                                 $nin: ['bug', 'feature']
                            }, votes: {$gt: 0}}, {
                            sort: {
                                votes: -1,
                                createdAt: -1
                            }, limit: 30
                        })
            };
            //console.log(to_return.item.fetch());
            return to_return;
    }

});

Router.route('/recent', {
    waitOn: function(){
        bonusMode.set('recent');
        return [IRLibLoader.load('http://bl.ocks.org/mbostock/raw/5616813/d3.geo.tile.min.js')]
    },
    template: 'reactivemap',
    name: "recent",
    data: function(){
            var to_return = {
                item: events.find({
                            type:{
                                 $nin: ['bug', 'feature']
                            }}, {
                            sort: {
                                createdAt: -1
                            }, limit: 30
                        })
            };
            //console.log(to_return.item.fetch());
            return to_return;
    }

});

Router.route('/bugs', {
    waitOn: function(){
        bonusMode.set('bugs');
        return [IRLibLoader.load('http://bl.ocks.org/mbostock/raw/5616813/d3.geo.tile.min.js')]
    },
    template: 'reactivemap',
    name: "bugs",
    data: function(){
            var to_return = {
                item: events.find({
                            type:{
                                 $in: ['bug', 'feature']
                            }}, {
                            sort: {
                                createdAt: -1
                            }
                        })
            };
            //console.log(to_return.item.fetch());
            return to_return;
    }
});




