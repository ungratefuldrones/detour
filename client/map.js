/*
    Template.map.onRendered(function() {
        
        var places = [{
            lng: this.data.lng,
            lat: this.data.lat
        }]

        var width = window.innerWidth,
            height = window.innerHeight;

        var tiler = d3.geo.tile()
            .size([width, height]);

        var projection = d3.geo.mercator()

        //offset center to make more legible 

        .center([this.data.lng, this.data.lat])
            .scale((1 << MAP_ZOOM) / 2 / Math.PI)
            .translate([width / 2, height / 2]);

        var path = d3.geo.path()
            .projection(projection);

        var svg = d3.select('#d3').append("svg")
            .attr("width", width)
            .attr("height", height);

        svg.selectAll("g")
            .data(tiler
                .scale(projection.scale() * 2 * Math.PI)
                .translate(projection([0, 0])))
            .enter().append("g")
            .each(function(d) {
                var g = d3.select(this);
                d3.json("http://vector.mapzen.com/osm/all/" + d[2] + "/" + d[0] + "/" + d[1] + ".json?api_key=vector-tiles-YCGoAMw", function(error, json) {
                    g.selectAll("path")
                        .data(json.features.sort(function(a, b) {
                            return a.properties.sort_key - b.properties.sort_key;
                        }))
                        .enter().append("path")
                        .attr("class", function(d) {
                            return d.properties.kind;
                        })
                        .attr("d", path);
                });
            });
        svg.selectAll(".pin")
            .data(places)
            .enter().append("circle", ".pin")
            .attr("r", 5)
            .attr("transform", function(d) {
                return "translate(" + projection([
                    d.lng,
                    d.lat
                ]) + ")";
            }).attr('class', 'map-pin');
    });
*/