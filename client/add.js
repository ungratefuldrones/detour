Template.add.events({
    'submit form#story': function(event, template) {
        event.preventDefault();
        event.stopPropagation();
        $("#save-story").prop('disabled', true);
        var toInsert = {};
        var geo = [];
        var location = $('#location').get(0).value; 
        var timestamp = +new Date();
        var file = $('#file').get(0).files[0];
        var body = $('#body_content')[0].innerHTML;
        var body_safe = $('#body_content')[0].innerText;
        var name = $('#name_content').get(0).value;
        var type = $('input[type="radio"]:checked:first').val();

        var toInsert = {
                    name: name,
                    body: body,
                    body_safe: body_safe,
                    createdAt: new Date(),
                    type: type
                    
        };

        if ( typeof location !== 'undefined' && location.length > 0 ) {
            Meteor.call('fetchFromOSM', location, function(error, result){
                if (error)  { 
                    if ($(".flash-error").hasClass("disabled")){
                        $(".flash-error").removeClass("disabled");
                    }
                    $(".flash-error span").text('Something went wrong. Please try again.'); 
                    $("#save-story").prop('disabled', false); 
                    throw error;
                }
                if (typeof file !== 'undefined'){
                    var fsFile = new FS.File(file);
                    fsFile.name(String(timestamp) + Meteor.default_connection._lastSessionId + '.' + fsFile.extension());
                    var fileObj = eventPhotos.insert(fsFile, function(err) {
                        if (err) {
                            $(".flash-error span").text('File type not allowed or too large.'); 
                            $("#save-story").prop('disabled', false);
                            throw err;
                        }
                    });
                    toInsert.file = {_id: fileObj._id};
                    toInsert.file_name = fsFile.name();
                }
                 try{
                    geo['lat'] = parseFloat(result.data[0].lat);
                    geo['lng'] = parseFloat(result.data[0].lon);
                } catch(e){
                    if ($(".flash-error").hasClass("disabled")){
                        $(".flash-error").removeClass("disabled");
                    }
                    $(".flash-error span").text('Unable to determine location. Please try again.');
                    $("#save-story").prop('disabled', false);
                    throw e;
                }
                toInsert.loc = { type: "Point", coordinates: [ geo['lng'], geo['lat'] ] };
                toInsert.address = result.data[0].address;
                toInsert.lat = geo['lat'];
                toInsert.lng = geo['lng'];


                events.insert( toInsert, function(err, res){
                    if (err) throw err;
                    bonusMode.set('none');
                    Router.go('item.single', {_id: res});

                });

            });
        } else if (Session.get('lat') && Session.get('lon')) {
            Meteor.call('fetchFromOSMReverse',[Session.get('lon'), Session.get('lat')], function(error, result){
                
                if (error) {
                    if ($(".flash-error").hasClass("disabled")){
                        $(".flash-error").removeClass("disabled");
                    }

                    $(".flash-error span").text('Something went wrong. Please try again.'); 
                    $(event.target).prop('disabled', false);
                    throw error;
                }
                if (typeof file !== 'undefined'){
                    var fsFile = new FS.File(file);
                    fsFile.name(String(timestamp) + Meteor.default_connection._lastSessionId + '.' + fsFile.extension());
                    var fileObj = eventPhotos.insert(fsFile, function(err) {
                        if (err) {
                            $(".flash-error span").text('File type not allowed or too large.'); 
                            $("#save-story").prop('disabled', false);
                            throw err;
                        }
                    });
                    toInsert.file = {_id: fileObj._id};
                    toInsert.file_name = fsFile.name();
                }
                try{
                    geo['lat'] = parseFloat(result.data.lat);
                    geo['lng'] = parseFloat(result.data.lon);
                }catch(e){
                    if ($(".flash-error").hasClass("disabled")){
                        $(".flash-error").removeClass("disabled");
                    }
                    $(".flash-error span").text('Unable to determine location. Please try again.');
                    $("#save-story").prop('disabled', false);
                    throw e;
                }
                toInsert.loc = { type: "Point", coordinates: [ geo['lng'], geo['lat'] ] };
                toInsert.address = result.data.address;
                toInsert.lat = geo['lat'];
                toInsert.lng = geo['lng'];

                events.insert( toInsert, function(err, res){
                    
                    if (err) throw err;
                    bonusMode.set('none');
                    Router.go('item.single', {_id: res});
                });

            });
        }  else {
            if ($(".flash-error").hasClass("disabled")){
                $(".flash-error").removeClass("disabled");
            }
            $(".flash-error span").text('Unable to save. Please try again.');
            $("#save-story").prop('disabled', false);

        }

    },
    'click form': function(event, template) {
        event.stopPropagation();
    },'click .help': function(event, template){
        event.preventDefault();
        Router.go("/help");
    },"input #fader": function(event, template){

    },
    'submit form#options': function(event, template){
        event.preventDefault();
        event.stopPropagation();
        Session.setPersistent('radius', $('#fader').val());
        Router.go("/nearby");

    }
});

Template.add.helpers({
    isActive: function() {
        return (bonusMode.get() == 'none' || bonusMode.get() == 'add');
    },
    optionForm: function() {
        return bonusMode.get() == 'options';
    },
    geoLocation: function(){
        return Geolocation.latLng();
    },
    geoEnabled: function(){
        return Session.get('lat') && Session.get('lon');
    },
    radius: function(){
        return Session.get('radius');
    }

});

Template.add.created = function(){
    function outputUpdate(val) {
        
    }
};


