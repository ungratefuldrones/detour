 var map, markers = undefined;

// Template.box.rendered = function () {
// 	//if (Mapbox.loaded()) {
// 	$.getScript('https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/leaflet.markercluster.js', function(){
// 	_.defer(function() {


//    	Deps.autorun(function () {
//    		//debugger;
// 		if (Deps.currentComputation.firstRun) {
				        
// 	        	console.log('fat');
// 				L.mapbox.accessToken = 'pk.eyJ1IjoiY2Jhcm9uYXZza2kiLCJhIjoiODA3MGQ2ZGRmZjgwN2YzZjA1ZmM1YjNmYzUxYzhmYjEifQ.axKblmSw8719IvxYXjtnqA';
// 				map = L.mapbox.map('the-map', 'cbaronavski.ciecxclj7002arvm8eenf042e', {
// 				       tileLayer: {format: 'jpg70'}
// 						}).setView([Session.get('lat'), Session.get('lon')], 13);
// 			    var coordinates = document.getElementById('coordinates');

// 				var marker = L.marker([Session.get('lat'), Session.get('lon')], {
// 				    icon: L.mapbox.marker.icon({
// 				      'marker-color': '#f86767'
// 				    }),
// 				    draggable: true
// 				}).addTo(map);

// 				// every time the marker is dragged, update the coordinates container
// 				marker.on('dragend', ondragend);

// 				// Set the initial marker coordinate on load.
// 				ondragend();

// 				function ondragend() {
// 				    var m = marker.getLatLng();
// 				    coordinates.innerHTML = 'Latitude: ' + m.lat + '<br />Longitude: ' + m.lng;
// 				}

//    		 } //end firstComputation


//         places = events.find({}, {
//             // sort: {
//             //     createdAt: -1
//             // }
//         }).fetch();

//         //console.log(places);
//         markers = undefined; 

// 	    markers = new L.MarkerClusterGroup();

// 	    for (var i = 0; i < places.length; i++) {
// 	        var a = places[i];
// 	        var title = a['name'];
// 	        var marker = L.marker(new L.LatLng(a.lat, a.lng), {
// 	            icon: L.mapbox.marker.icon({'marker-color': '0044FF'}),
// 	            title: title
// 	        });
// 	        marker.bindPopup(title);
// 	        markers.addLayer(marker);
// 	    }

// 	    map.addLayer(markers);


//     	}); //end autorun
// 		}); //end getScript
// 	}); // end defer
// 	//}
// };

Template.box.rendered = function () {
	//if (Mapbox.loaded()) {

	_.defer(function() {
	$.getScript("https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/leaflet.markercluster.js", function() {

   	Deps.autorun(function () {
   		//debugger;
		if (Deps.currentComputation.firstRun) {
				        
	        	console.log('fat');
				L.mapbox.accessToken = 'pk.eyJ1IjoiY2Jhcm9uYXZza2kiLCJhIjoiODA3MGQ2ZGRmZjgwN2YzZjA1ZmM1YjNmYzUxYzhmYjEifQ.axKblmSw8719IvxYXjtnqA';
				map = L.mapbox.map('the-map', 'cbaronavski.ciecxclj7002arvm8eenf042e', {
				       tileLayer: {format: 'jpg70'}
						}).setView([Session.get('lat'), Session.get('lon')], 13);
			    var coordinates = document.getElementById('coordinates');

				var marker = L.marker([Session.get('lat'), Session.get('lon')], {
				    icon: L.mapbox.marker.icon({
				      'marker-color': '#f86767'
				    }),
				    draggable: true
				}).addTo(map);

				// every time the marker is dragged, update the coordinates container
				marker.on('dragend', ondragend);

				// Set the initial marker coordinate on load.
				ondragend();

				function ondragend() {
				    var m = marker.getLatLng();
				    coordinates.innerHTML = 'Latitude: ' + m.lat + '<br />Longitude: ' + m.lng;
				}
				markers = new L.MarkerClusterGroup();
   		 } //end firstComputation


        places = events.find({}, {
            // sort: {
            //     createdAt: -1
            // }
        }).fetch();

        //console.log(places);

		markers = undefined;
		markers = new L.MarkerClusterGroup();

	    

	    for (var i = 0; i < places.length; i++) {
	        var a = places[i];
	        var title = a['name'];
	        var marker = L.marker(new L.LatLng(a.lat, a.lng), {
	            icon: L.mapbox.marker.icon({'marker-color': '0044FF'}),
	            title: title
	        });
	        marker.bindPopup(title);
	        markers.addLayer(marker);
	    }

	    map.addLayer(markers);


    	}); //end autorun
		}); // end getScript
	}); // end defer
	//}
};


