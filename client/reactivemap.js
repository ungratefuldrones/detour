Template.reactivemap.events({
    "click circle.map-pin": function(event, template) {
        try{
            Router.go('cluster', {_id:event.target.__data__.properties.FIPS});
        } catch(e){
            Router.go('item.single', {_id:event.target.__data__._id});

        }
    },
    "click #add": function(event, template) {
        event.stopPropagation();
        history.back();
    }
});

Template.reactivemap.helpers({
    isActive: function() {
        return  ['none', 'add', 'help', 'hud', 'options'].indexOf(bonusMode.get()) === -1; //(bonusMode.get() != 'none' && bonusMode.get() != 'add' && bonusMode.get() != 'help' && bonusMode.get() !='hud' && bon);
    },
    addItem: function() {
        return bonusMode.get() == 'add' || bonusMode.get() == 'options';
    },
    isHelp: function() {
        return bonusMode.get() == 'help';
    },
    isHud: function() {
        return bonusMode.get() == 'hud';
    },
    isCluster: function() {
      return typeof bonusMode.get().cluster !== 'undefined' || ['nearby', 'recent', 'trending', 'bugs'].indexOf(bonusMode.get()) > -1;
    }


});


// voronoi labels/ dots to prevent occlusion - prevent dot overlap
Template.reactivemap.rendered = function() {
    window.brooklyn = [-73.975536, 40.691674];

    $(window).on('resize', function() {
        // resize map elements
        window.detour_map.width = window.innerWidth;
        window.detour_map.height = window.innerHeight;
        $('#d3 .map').css({
            'width': window.detour_map.width,
            'height': window.detour_map.height
        });
        $('#d3 .pins').css({
            'width': window.detour_map.width,
            'height': window.detour_map.height
        });
        $('#d3 .places').css({
            'width': window.detour_map.width,
            'height': window.detour_map.height
        });
        window.detour_map.tiler = d3.geo.tile()
            .size([window.detour_map.width, window.detour_map.height]);
        zoomed();

    });

    var key = function(d) {
        return d._id;
    };

    function zoomed() {
        // console.log(window.detour_map.zoom.scale() / 2 / Math.PI);
        // console.log(window.detour_map.zoom.translate());
        // console.log(window.detour_map.projection([77.044092,-38.907631]));
        window.detour_map.tiles = window.detour_map.tiler
            .scale(window.detour_map.zoom.scale())
            .translate(window.detour_map.zoom.translate())
            ();

        window.detour_map.projection
            .scale(window.detour_map.zoom.scale() / 2 / Math.PI)
            .translate(window.detour_map.zoom.translate());

        window.detour_map.image = window.detour_map.layer
            .style(window.detour_map.prefix + "transform", matrix3d(window.detour_map.tiles.scale, window.detour_map.tiles.translate))
            .selectAll(".tile")
            .data(window.detour_map.tiles, function(d) {
                return d;
            });

        window.detour_map.image.exit()
            .each(function(d) {
                this._xhr.abort();
            })
            .remove();

        //window.detour_map.places.selectAll('g').exit().remove();

        window.detour_map.image.enter().append("svg")
            .attr("class", "tile")
            .style(
                    {"left": function(d) {
                return d[0] * 256 + "px";
            },
            "top": function(d) {
                return d[1] * 256 + "px";
            }})
            .each(function(d) {

                var svg = d3.select(this);
                //http://localhost:8080/osm2/
                //                this._xhr = d3.json("http://" + ["a", "b", "c"][(d[0] * 31 + d[1]) % 3] + ".tile.openstreetmap.us/vectiles-highroad/" + d[2] + "/" + d[0] + "/" + d[1] + ".json", function(error, json) {

                // this._xhr = d3.json("https://vector.mapzen.com/osm/all/" + d[2] + "/" + d[0] + "/" + d[1] + ".topojson?api_key=vector-tiles-LM25tq4", function(error, json) {
                //   var k = Math.pow(2, d[2]) * 256; // size of the world in pixels
                //   tilePath.projection()
                //       .translate([k / 2 - d[0] * 256, k / 2 - d[1] * 256]) // [0°,0°] in pixels
                //       .scale(k / 2 / Math.PI);

                //   var data = {};
                //   for (var key in json.objects) {
                //     data[key] = topojson.feature(json, json.objects[key]);
                //   }

                //   layers.forEach(function(layer){
                //     var layer_data = data[layer];
                //     if (layer_data) {
                //       svg.selectAll("path")
                //         .data(layer_data.features.sort(function(a, b) { return a.properties.sort_key ? a.properties.sort_key - b.properties.sort_key : 0 }))
                //       .enter().append("path")
                //         .attr("class", function(d) { var kind = d.properties.kind || ''; return layer + '-layer ' + kind; })
                //         .attr("d", tilePath);
                //     }
                //   });
                // });
                var layers = ['water', 'roads', 'places'];

                var place_type = [];
                if (window.detour_map.zoom.scale() < 1 << 22) {
                    place_type = ['Populated place', 'city', 'Admin-0 capital'];
                } else {
                    place_type = ['Populated place', 'neighborhood', 'city', 'suburb', 'hamlet', 'Admin-0 capital', 'village', 'town'];
                }

                this._xhr = d3.json("http://vector.mapzen.com/osm/all/" + d[2] + "/" + d[0] + "/" + d[1] + ".topojson?api_key=vector-tiles-YCGoAMw", function(error, json) {

                    var k = Math.pow(2, d[2]) * 256; // size of the world in pixels

                    window.detour_map.tilePath.projection()
                        .translate([k / 2 - d[0] * 256, k / 2 - d[1] * 256]) // [0°,0°] in pixels
                        .scale(k / 2 / Math.PI);

                    var data = {};
                    for (var key in json.objects) {
                        data[key] = topojson.feature(json, json.objects[key]);
                    }

                    layers.forEach(function(layer) {
                        var layer_data = data[layer];
                        if (layer != 'places') {
                            svg.selectAll("text")
                                .data(layer_data.features.sort(function(a, b) {
                                    return a.properties.sort_key ? a.properties.sort_key - b.properties.sort_key : 0
                                }))
                                .enter().append("path")
                                .attr({
                                    "class": function(d) {
                                        return d.properties.kind;
                                    },
                                    "d": window.detour_map.tilePath
                                });
                        }


                        // else if (layer_data) {
                        //   svg.selectAll("text")
                        //     .data(layer_data.features.filter(function(d){return place_type.indexOf(d.properties.kind) > -1;   }).sort(function(a, b) { return a.properties.sort_key ? a.properties.sort_key - b.properties.sort_key : 0 }))

                        //     .enter().append("text")
                        //     .text(function(d) {
                        //         return d.properties['name']
                        //     })
                        //     .attr('class', function(d) {
                        //         return d.properties.kind;
                        //     })
                        //     .attr('x', function(d) {
                        //         return window.detour_map.tileProjection(d.geometry.coordinates)[0]
                        //     })
                        //     .attr('y', function(d) {
                        //         return window.detour_map.tileProjection(d.geometry.coordinates)[1]
                        //     }).style('font-size', function(d){return (window.detour_map.zoom.scale() >> 25) + 2 + 'em'; });
                        // }
                    });


                    // svg.selectAll("path")
                    //     .data(json.water.features)
                    //     .enter().append("path")
                    //     .attr("class", function(d) {
                    //         return d.properties.kind;
                    //     })
                    //     .attr("d", window.detour_map.tilePath);

                    // svg.selectAll("path")
                    //     .data(json.buildings.features)
                    //     .enter().append("path")
                    //     .attr("class", function(d) {
                    //         return 'building'
                    //     })
                    //     .attr("d", window.detour_map.tilePath);

                    // svg.selectAll("path")
                    //     .data(json.roads.features)
                    //     .enter().append("path")
                    //     .attr("class", function(d) {
                    //         return d.properties.kind;
                    //     })
                    //     .attr("d", window.detour_map.tilePath);



                    // at this point, do some magic to prevent overlapping labels

                    // svg.selectAll("text")
                    //     .data(json.objects.places.geometries.filter(function(d){return place_type.indexOf(d.properties.kind) > -1;   }))
                    //     .enter().append("text")
                    //     .text(function(d) {
                    //         return d.properties['name']
                    //     })
                    //     .attr('class', function(d) {
                    //         return d.properties.kind;
                    //     })
                    //     .attr('x', function(d) {
                    //         return window.detour_map.tileProjection(d.geometry.coordinates)[0]
                    //     })
                    //     .attr('y', function(d) {
                    //         return window.detour_map.tileProjection(d.geometry.coordinates)[1]
                    //     }).style('font-size', function(d){return (window.detour_map.zoom.scale() >> 25) + 2 + 'em'; });

                });

            });

        zoom_pins();
    }

    function zoom_pins() {
        if (typeof window.detour_map.pin_selector === 'undefined') {
            return;
        }

        d3.selectAll("circle.map-pin")
            .each(function(d, i) {
                var coordinates = [];
                try{
                    coordinates = d.geometry.coordinates; 
                } catch (e){
                    coordinates = d.loc.coordinates;
                }
                coords = window.detour_map.projection(coordinates);
                d3.select(this).attr({
                    'cy': coords[1],
                    'cx': coords[0]
                })

        });

        // d3.selectAll("text.map-pin")
        //     .each(function(d, i) {
        //         coords = window.detour_map.projection(d.geometry.coordinates);
        //         d3.select(this).attr({
        //             'y': coords[1],
        //             'x': coords[0]
        //         })

        //     });
    }

    function mousemoved() {
        //window.detour_map.info.text(formatLocation(window.detour_map.projection.invert(d3.mouse(this)), window.detour_map.zoom.scale()));
    }

    function matrix3d(scale, translate) {
        var k = scale / 256,
            r = scale % 1 ? Number : Math.round;
        return "matrix3d(" + [k, 0, 0, 0, 0, k, 0, 0, 0, 0, k, 0, r(translate[0] * scale), r(translate[1] * scale), 0, 1] + ")";
    }

    function prefixMatch(p) {
        var i = -1,
            n = p.length,
            s = document.body.style;
        while (++i < n)
            if (p[i] + "Transform" in s) return "-" + p[i].toLowerCase() + "-";
        return "";
    }

    function formatLocation(p, k) {
            var format = d3.format("." + Math.floor(Math.log(k) / 2 - 2) + "f");
            return (p[1] < 0 ? format(-p[1]) + "°S" : format(p[1]) + "°N") + " " + (p[0] < 0 ? format(-p[0]) + "°W" : format(p[0]) + "°E");
        }
        // We need to wait until the DOM is loaded, so we use defer
    _.defer(function() {

        window.detour_map = {} // Create chart element in window namespace

        // We make this Reactive using Deps.autorun
        Deps.autorun(function() {



            // Theres some stuff we only want to run once
            if (Deps.currentComputation.firstRun) {
                // make this better
                var loc_init = Session.get('lon') && Session.get('lat') ? [Session.get('lon'), Session.get('lat')] : [-77.044092,38.907631];
                window.detour_map.width = window.innerWidth;
                window.detour_map.height = window.innerHeight;
                window.detour_map.prefix = prefixMatch(["webkit", "ms", "Moz", "O"]);

                window.detour_map.tiler = d3.geo.tile()
                    .size([window.detour_map.width, window.detour_map.height]);

                window.detour_map.projection = d3.geo.mercator()
                    .scale((1 << 20) / 2 / Math.PI)
                    .translate([-window.detour_map.width / 2, -window.detour_map.height / 2]); // just temporary

                window.detour_map.tileProjection = d3.geo.mercator();

                window.detour_map.tilePath = d3.geo.path()
                    .projection(window.detour_map.tileProjection);

                window.detour_map.zoom = d3.behavior.zoom()
                    .scale(window.detour_map.projection.scale() * 2 * Math.PI)
                    .scaleExtent([1 << 10, 1 << 24])
                    .translate(window.detour_map.projection(loc_init).map(function(x) {
                        return -x;
                    }))
                    .on("zoom", zoomed);

                window.detour_map.map = d3.select("#d3").append("div")
                    .attr("class", "map")
                    .style("width", window.detour_map.width + "px")
                    .style("height", window.detour_map.height + "px")
                    .call(window.detour_map.zoom)
                    .on("mousemove", mousemoved);

                window.detour_map.layer = window.detour_map.map.append("div")
                    .attr("class", "layer");

                // window.detour_map.info = window.detour_map.map.append("div")
                //     .attr("class", "map-info")
                //     .append('button')
                //     .attr('class', 'reset')
                //     .text('Reset');

                window.detour_map.pins = window.detour_map.map.append("div").attr('class', 'pins').append("svg")
                    .attr('class', 'pins')
                    .style("width", window.detour_map.width + "px")
                    .style("height", window.detour_map.height + "px");

                // window.detour_map.pins_single = window.detour_map.map.append("div").attr('class', 'pins').append("svg")
                //     .attr('class', 'pins')
                //     .style("width", window.detour_map.width + "px")
                //     .style("height", window.detour_map.height + "px");


                window.detour_map.places = window.detour_map.map.append("div").attr('class', 'places').append("svg")
                    .attr('class', 'places')
                    .style("width", window.detour_map.width + "px")
                    .style("height", window.detour_map.height + "px");

                window.detour_map.colors = d3.scale.category10();

                window.detour_map.zoomTo = function() {
                    var location = window.brooklyn;
                    var newscale = 1 << 20;
                    window.detour_map.zoom
                        .translate(window.detour_map.projection(location).map(function(x) {
                            return -x;
                        }))
                        .scale((window.detour_map.projection.scale()) * 2 * Math.PI);
                };

                zoomed();


                $('.reset').on('click', function() {
                event.preventDefault;
                var geo = [Session.get('lon'), Session.get('lat')];
                var point = window.detour_map.projection([-77.044092,38.907631]);

                window.detour_map.zoom.translate([-point[0] * window.detour_map.zoom.scale() / 2 / Math.PI, -point[1] * window.detour_map.zoom.scale() / 2 / Math.PI]).scale(window.detour_map.zoom.scale() / 2 / Math.PI);
                zoomed();
                });


                // function reset() {
                //   d3.transition().duration(750).tween("zoom", function() {
                //     var ix = d3.interpolate(x.domain(), [-width / 2, width / 2]),
                //         iy = d3.interpolate(y.domain(), [-height / 2, height / 2]);
                //     return function(t) {
                //       zoom.x(x.domain(ix(t))).y(y.domain(iy(t)));
                //       zoomed();
                //     };
                //   });
                // }
            }

            places = clusters.find({}, {
                // sort: {
                //     createdAt: -1
                // }
            }).fetch();

            singletons = events.find({cluster: {$exists: false}}).fetch();
            

            // places = events.find({}, {
            //     sort: {
            //         createdAt: -1
            //     }
            // }).fetch();

            window.detour_map.pin_selector = window.detour_map.pins.selectAll("circle.map-pin.cluster").data(places, key);
            window.detour_map.pins_to_remove = window.detour_map.pin_selector.exit();
            //window.detour_map.label_selector = window.detour_map.pins.selectAll("text.map-pin").data(places, key);
            // window.detour_map.labels_to_remove = window.detour_map.label_selector.exit();
            window.detour_map.pins_to_remove.remove();
            // window.detour_map.labels_to_remove.remove();
            window.detour_map.pin_selector.enter().append("circle", ".map-pin.cluster")
                .attr({
                    r: function(d) {
                        return 12 + Math.log(d.properties.items.length);
                    },
                    "data-id": function(d) {
                        return d._id;
                    },
                    class: 'map-pin cluster'
                });

            window.detour_map.pin_single_selector = window.detour_map.pins.selectAll("circle.map-pin.single").data(singletons, key);
            window.detour_map.pins_single_to_remove = window.detour_map.pin_single_selector.exit();
            // window.detour_map.label_selector = window.detour_map.pins.selectAll("text.map-pin").data(places, key);
            // window.detour_map.labels_to_remove = window.detour_map.label_selector.exit();
            window.detour_map.pins_single_to_remove.remove();
            // window.detour_map.labels_to_remove.remove();
            window.detour_map.pin_single_selector.enter().append("circle", ".map-pin.single")
                .attr({
                    r: function(d) {
                        return 12;
                    },
                    "data-id": function(d) {
                        return d._id;
                    },
                    class: 'map-pin single'
                });







            // window.detour_map.pin_single_selector = window.detour_map.pins_single.selectAll("circle.map-pin.single").data(singletons, key);
            // window.detour_map.pins_single_to_remove = window.detour_map.pin_single_selector.exit();
            // // window.detour_map.label_selector = window.detour_map.pins.selectAll("text.map-pin").data(places, key);
            // // window.detour_map.labels_to_remove = window.detour_map.label_selector.exit();
            // window.detour_map.pins_single_to_remove.remove();
            // // window.detour_map.labels_to_remove.remove();
            // window.detour_map.pin_single_selector.enter().append("circle", ".map-pin")
            //     .attr({
            //         r: function(d) {
            //             return 12;
            //         },
            //         "data-id": function(d) {
            //             return d._id;
            //         },
            //         class: "map-pin"
            //     });








            // window.detour_map.label_selector.enter().append("text", ".map-pin")
            //     .attr("data-id", function(d) {
            //         return d._id;
            //     })
            //     .text(function(d) {
            //         return d.properties.items.length;
            //     })
            //     .attr('class', 'map-pin');

            zoom_pins();
        });

    });
};
