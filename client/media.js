var multimediaEvents = {
	"click .multimedia": function(event, template) {
	    event.stopImmediatePropagation();
	    event.preventDefault();
	    multimediaController.set(this.copies.eventPhotos.key);
      if($(event.target).hasClass('playing')){
        window.player.pause();
        audioState.set('pause');
      } else{
        window.player.play();
        audioState.set('play');
      }
  	}
};

var multimediaHelpers = {
    video_ready: function() {
        return !!this.ready && Template.parentData().video_ready;
    },
    isPlaying: function() {
        return multimediaController.get() == this.copies.eventPhotos.key  && audioState.get() == 'play';
    }
};

Template.mediaAsset.events(multimediaEvents);
Template.mediaAsset.helpers(multimediaHelpers);

Template.thumbAsset.events(multimediaEvents);
Template.thumbAsset.helpers(multimediaHelpers);

Template.listenWatch.events(multimediaEvents);
Template.listenWatch.helpers(multimediaHelpers);
Template.listenMain.events(multimediaEvents);
Template.listenMain.helpers(multimediaHelpers);

Template.videoTemplate.onRendered(function() {
    
    var that = this;
    this.autorun(function() {
      if (JWPlayer.loaded()) {
          jwplayer('video-' + that.data._id ).setup({
              file: media_root + that.data.copies.eventPhotos.key,
              primary: "flash",
              width:'100%',
              aspectratio: "16:9"
          });
      }
    });
    
});

var multimedia_helper = {
    audioPlayer: function(){
      return multimediaController.get() != 'none';
    },
    audioFile: function(){
      return multimediaController.get();
    }, isPlaying: function(){
      return audioState.get() == 'play';
    }, murl : function(){
        return '/cfs/files/eventPhotos/' + this._id + '/' + this.original['name'];
    }, slider: function(){
      return bonusMode.get() == 'nearby';
    }, radius: function(){
      return parseInt(radius.get());
  }
};

Template.audioTemplate.events({
  "click #play": function(event, template){
      window.player.play();

  },
  "click #stop": function(event, template){
      audioState.set('stop');
      window.player.pause();
  }, 
  "click .playbar": function(event, template){
      window.player.seek(event.offsetX / event.currentTarget.clientWidth * window.player.duration());
  }
});

Template.footer.helpers({  
    thisComment: function(){
       return commentMode.get() == bonusMode.get().item;
  },comment: function(){
        return commentMode.get();
  }
});


Template.audioTemplate.created = function(){
    $('.playbar-overlay').off();
};


Template.footer.helpers(multimedia_helper);
Template.footer.events({
  "input input[type='range']": function(event,template){
    //Session.setPersistent('radius', $('input[name="rangeValue"]').val());
    radius.set($('input[name="rangeValue"]').val());
  }
});

Template.audioTemplate.helpers(multimedia_helper);

Template.videoTemplate.helpers(multimedia_helper);